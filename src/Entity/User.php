<?php

namespace App\Entity;

use Carbon\Carbon;

class User{

    public $email, $nom, $prenom, $dateNaissance;

    public function __construct($email, $nom, $prenom, $dateNaissance){
        $this->email = $email;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->dateNaissance = $dateNaissance;
    }

    public function isValid() : bool {
        $age = $this->dateNaissance
            ->diff(new Carbon('now'))
            ->y;
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL) || $age < 13) {
            return false;
        }
        return true;
    }

}

