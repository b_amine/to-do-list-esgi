<?php

namespace App\Tests;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use App\Entity\User;


class UserTest extends TestCase
{
    public function testIsValid(){
        $user = new User("test@gmail.com","nom","prenom",new Carbon("27-06-1996"));
        $this->assertEquals(true, $user->isValid());

        $user = new User("test@gmail0test.com","nom","prenom",new Carbon("27-06-1996"));
        $this->assertEquals(true, $user->isValid());

        $user = new User("test@gmail.com","nom","prenom",new Carbon("27-06-2015"));
        $this->assertEquals(false, $user->isValid());

        $user = new User("testatgmail.com","nom","prenom",new Carbon("27-06-1996"));
        $this->assertEquals(false, $user->isValid());


    }

}
